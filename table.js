jQuery(document).ready(function($) {

	// Fetch, process and display geoJSON.
	$.when(
		$.getJSON("/"+getLang(false)+"/geojson", {}) 
		.done (function( data ) {

			$.each( data.features, function( id, feature ) {
				var item = feature.properties;
				//console.log(feature.properties);

				//var date = item.date.substring(0,2)+"/"+item.date.substring(3,5)+"/"+item.date.substring(6); 
				var timestamp = new Date(item.date.substring(6),item.date.substring(3,5),item.date.substring(0,2)).getTime()

				$('<tr id="'+item.id+'">\
					<td class="'+item.catSlug+'"><span>'+item.category+'</span></td>\
					<td><a target="_parent" href="/index.php?p='+item.id+'">'+item.title+'</a></td>\
					<td data-sort="'+timestamp+'">'+item.date+'</td>\
					<td>'+item.city+'</td>\
					<td>'+item.province+'</td>\
					<td>'+item.sentence+'</td>\
					<td>'+item.delict+'</td>\
				</tr>').appendTo($("#crims tbody"));
					//<td>'+feature.geometry.coordinates[1]+'</td>\
					//<td>'+feature.geometry.coordinates[0]+'</td>\
					//<td>'+item.age+'</td>\
			});

			$('#crims').dataTable( {
				"aLengthMenu": [[20, 50, 100, -1], [20, 50, 100, "All"]],
				"iDisplayLength": 20,
				"aaSorting": [[ 2, "desc" ]],
				"sPaginationType": "full_numbers",
				"language": {
	                "url": "/wp-content/plugins/hatecrimes-table/lib/dataTables."+getLang()+".lang"
	            },
				initComplete: function () {
					var table = this;
		            this.api().columns().every( function (i) {
		            	if (i === 0) {
			                var column = this;
							// filter buttons
							$(".filter-btn").click(function() {
								//console.log($(this).text());
								column.search($(this).text()).draw();
								setParam($(this).text());
							});

							$(".filters-mobile .type").on( 'change', function () {
		                    	if ($(this).val() !== null) {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );
			                        column
			                            .search( val )
			                            .draw();
			                        setParam(val);
			                    }
		                    } );

		       				// apply initial GET parameter
							var type = findGetParameter("type");
						    if (type) {
						    	column.search(type).draw();
							}
			            }
		            	else if (i === 2) {
		            		// date, show only year value
		            		// type, split up column data and search for containing value
			                var column = this;
		            		
			                // unbind click handler
		            		//$(column.header()).off("click");

			                var select = $('<select><option value=""></option></select>')
			                    .appendTo( $(column.header()) )
			                    .on( 'change', function () {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );

			                        column
			                            .search( val )
			                            .draw();
			                    } );
			 
			                var data = getYears(column.data().unique().sort());
			                for (key in data) {
			                	select.append( '<option value="'+data[key]+'">'+data[key]+'</option>' )
			                }
		            	}
		            	else if (i > 2) {
		            		// all other columns, search for exact value
			                var column = this;
			                var select = $('<select><option value=""></option></select>')
			                    .appendTo( $(column.header()) )
			                    .on( 'change', function () {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );
			 
			                        column
			                            .search( val ? '^'+val+'$' : '', true, false )
			                            .draw();
			                    } );
			 
			                column.data().unique().sort().each( function ( d, j ) {
			                    select.append( '<option value="'+d+'">'+d+'</option>' )
			                } );
			            }
		            } );

					// reset filters = show all registers
					$(".filter-btn-all").click(function() {
						table
							.api()
							.search( '' )
							.columns().search( '' )
							.draw();
	
						setParam("");
					});
				}
			});
		})
	    .fail(function(data) {    
	      console.log("json error");
	    })
	).then(function() { 
		console.log("json loaded!");
	});

	//get language from URL
	function getLang(long=true) {
		var language = "Catalan";
		if (!long) language = "ca";
		var loc = window.location.href;
		var url = "crimenesdeodio.info/"
		var pos1 = loc.indexOf(url);
		loc = loc.substring(pos1+url.length);
		loc = loc.split("/");
		if (loc[0] == "en") {
			language = "English";
			if (!long) language = "en";
		} else if (loc[0] == "es") {
			language = "Spanish";
			if (!long) language = "es";
		}
		return language;
	}

	function splitContent(data) {
		var result = {};
		data.each( function ( d, j ) {
			d.split(',').forEach(function(ele) {
				result[ele.trim()] = ele.trim();
			});
		});

		// falta sort!
		return result;
	}

	function getYears(data) {
		var result = {};
		data.each( function ( d, j ) {
			result[d.substring(6)] = d.substring(6);
		});

		// falta sort!
		return result;
	}

	function setParam(param="") {
		if (param != "") {
			param = "?type="+param;
		}
	    var oPageInfo = {
			title: "Search Results",
			url: "https://crimenesdeodio.info/ca/casos/" + param
	    }
	    window.history.replaceState(oPageInfo, oPageInfo.title, oPageInfo.url);
	}

	function findGetParameter(parameterName) {
	    var result = null,
	        tmp = [];
	    location.search
	    .substr(1)
	        .split("&")
	        .forEach(function (item) {
	        tmp = item.split("=");
	        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	    });
	    return result;
	}
});